(defproject clj-gitlab "0.2.1-SNAPSHOT"
  :description "Client for GitLab API"
  :url "https://gitlab.com/dzaporozhets/clj-gitlab"
  :pom-addition [:distributionManagement
                 [:snapshotRepository
                  [:id "gitlab-maven"]
                  [:url "https://gitlab.com/api/v4/projects/4186769/packages/maven"]]
                 [:repository
                  [:id "gitlab-maven"]
                  [:url "https://gitlab.com/api/v4/projects/4186769/packages/maven"]]]

  :license {:name "MIT"
            :url "https://choosealicense.com/licenses/mit/"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [cheshire "5.8.1"]
                 [clj-http "3.10.0"]]
  :profiles {:dev {:dependencies [[com.gfredericks/vcr-clj "0.4.18"]]
                   :plugins [[lein-kibit "0.1.5"]
                             [lein-ancient "0.6.15"]]}})

{:calls [{:arg-key {:query-string nil,
                    :request-method :get,
                    :server-name "gitlab.com",
                    :server-port nil,
                    :uri "/api/v4/projects/4202845/issues"},
          :return {:body #vcr-clj/input-stream
                   ["W3siaWQiOjY5MDYyMjIsImlpZCI6MywicHJvamVjdF9pZCI6NDIwMjg0NSwidGl0bGUiOiJPcGV"
                    "uIGFzc2lnbmVkIGlzc3VlIiwiZGVzY3JpcHRpb24iOiIiLCJzdGF0ZSI6Im9wZW5lZCIsImNyZW"
                    "F0ZWRfYXQiOiIyMDE3LTA5LTIxVDEzOjMxOjI2LjEyOFoiLCJ1cGRhdGVkX2F0IjoiMjAxNy0wO"
                    "S0yMVQxMzozMToyOS45MzFaIiwibGFiZWxzIjpbXSwibWlsZXN0b25lIjpudWxsLCJhc3NpZ25l"
                    "ZXMiOlt7ImlkIjozODUyNywibmFtZSI6IkRvZ2UgRGV2IiwidXNlcm5hbWUiOiJkb2dlZGV2Iiw"
                    "ic3RhdGUiOiJhY3RpdmUiLCJhdmF0YXJfdXJsIjoiaHR0cHM6Ly9nbC1zdGF0aWMuZ2xvYmFsLn"
                    "NzbC5mYXN0bHkubmV0L3VwbG9hZHMvLS9zeXN0ZW0vdXNlci9hdmF0YXIvMzg1MjcvZG9nZS5qc"
                    "GVnIiwid2ViX3VybCI6Imh0dHBzOi8vZ2l0bGFiLmNvbS9kb2dlZGV2In1dLCJhdXRob3IiOnsi"
                    "aWQiOjM4NTI3LCJuYW1lIjoiRG9nZSBEZXYiLCJ1c2VybmFtZSI6ImRvZ2VkZXYiLCJzdGF0ZSI"
                    "6ImFjdGl2ZSIsImF2YXRhcl91cmwiOiJodHRwczovL2dsLXN0YXRpYy5nbG9iYWwuc3NsLmZhc3"
                    "RseS5uZXQvdXBsb2Fkcy8tL3N5c3RlbS91c2VyL2F2YXRhci8zODUyNy9kb2dlLmpwZWciLCJ3Z"
                    "WJfdXJsIjoiaHR0cHM6Ly9naXRsYWIuY29tL2RvZ2VkZXYifSwiYXNzaWduZWUiOnsiaWQiOjM4"
                    "NTI3LCJuYW1lIjoiRG9nZSBEZXYiLCJ1c2VybmFtZSI6ImRvZ2VkZXYiLCJzdGF0ZSI6ImFjdGl"
                    "2ZSIsImF2YXRhcl91cmwiOiJodHRwczovL2dsLXN0YXRpYy5nbG9iYWwuc3NsLmZhc3RseS5uZX"
                    "QvdXBsb2Fkcy8tL3N5c3RlbS91c2VyL2F2YXRhci8zODUyNy9kb2dlLmpwZWciLCJ3ZWJfdXJsI"
                    "joiaHR0cHM6Ly9naXRsYWIuY29tL2RvZ2VkZXYifSwidXNlcl9ub3Rlc19jb3VudCI6MCwidXB2"
                    "b3RlcyI6MCwiZG93bnZvdGVzIjowLCJkdWVfZGF0ZSI6bnVsbCwiY29uZmlkZW50aWFsIjpmYWx"
                    "zZSwid2VpZ2h0IjpudWxsLCJ3ZWJfdXJsIjoiaHR0cHM6Ly9naXRsYWIuY29tL2RvZ2VkZXYvdG"
                    "VzdC1jbGktZ2l0bGFiL2lzc3Vlcy8zIiwidGltZV9zdGF0cyI6eyJ0aW1lX2VzdGltYXRlIjowL"
                    "CJ0b3RhbF90aW1lX3NwZW50IjowLCJodW1hbl90aW1lX2VzdGltYXRlIjpudWxsLCJodW1hbl90"
                    "b3RhbF90aW1lX3NwZW50IjpudWxsfX0seyJpZCI6NjkwNjA1NywiaWlkIjoyLCJwcm9qZWN0X2l"
                    "kIjo0MjAyODQ1LCJ0aXRsZSI6IkNsb3NlZCBpc3N1ZSIsImRlc2NyaXB0aW9uIjoiIiwic3RhdG"
                    "UiOiJjbG9zZWQiLCJjcmVhdGVkX2F0IjoiMjAxNy0wOS0yMVQxMzoxODo0MC45NzZaIiwidXBkY"
                    "XRlZF9hdCI6IjIwMTctMDktMjFUMTM6MTg6NDMuODk5WiIsImxhYmVscyI6W10sIm1pbGVzdG9u"
                    "ZSI6bnVsbCwiYXNzaWduZWVzIjpbXSwiYXV0aG9yIjp7ImlkIjozODUyNywibmFtZSI6IkRvZ2U"
                    "gRGV2IiwidXNlcm5hbWUiOiJkb2dlZGV2Iiwic3RhdGUiOiJhY3RpdmUiLCJhdmF0YXJfdXJsIj"
                    "oiaHR0cHM6Ly9nbC1zdGF0aWMuZ2xvYmFsLnNzbC5mYXN0bHkubmV0L3VwbG9hZHMvLS9zeXN0Z"
                    "W0vdXNlci9hdmF0YXIvMzg1MjcvZG9nZS5qcGVnIiwid2ViX3VybCI6Imh0dHBzOi8vZ2l0bGFi"
                    "LmNvbS9kb2dlZGV2In0sImFzc2lnbmVlIjpudWxsLCJ1c2VyX25vdGVzX2NvdW50IjowLCJ1cHZ"
                    "vdGVzIjowLCJkb3dudm90ZXMiOjAsImR1ZV9kYXRlIjpudWxsLCJjb25maWRlbnRpYWwiOmZhbH"
                    "NlLCJ3ZWlnaHQiOm51bGwsIndlYl91cmwiOiJodHRwczovL2dpdGxhYi5jb20vZG9nZWRldi90Z"
                    "XN0LWNsaS1naXRsYWIvaXNzdWVzLzIiLCJ0aW1lX3N0YXRzIjp7InRpbWVfZXN0aW1hdGUiOjAs"
                    "InRvdGFsX3RpbWVfc3BlbnQiOjAsImh1bWFuX3RpbWVfZXN0aW1hdGUiOm51bGwsImh1bWFuX3R"
                    "vdGFsX3RpbWVfc3BlbnQiOm51bGx9fSx7ImlkIjo2OTA2MDU0LCJpaWQiOjEsInByb2plY3RfaW"
                    "QiOjQyMDI4NDUsInRpdGxlIjoiT3BlbiBpc3N1ZSIsImRlc2NyaXB0aW9uIjoiIiwic3RhdGUiO"
                    "iJvcGVuZWQiLCJjcmVhdGVkX2F0IjoiMjAxNy0wOS0yMVQxMzoxODozNC44NDJaIiwidXBkYXRl"
                    "ZF9hdCI6IjIwMTctMDktMjFUMTM6MTg6MzQuODQyWiIsImxhYmVscyI6W10sIm1pbGVzdG9uZSI"
                    "6bnVsbCwiYXNzaWduZWVzIjpbXSwiYXV0aG9yIjp7ImlkIjozODUyNywibmFtZSI6IkRvZ2UgRG"
                    "V2IiwidXNlcm5hbWUiOiJkb2dlZGV2Iiwic3RhdGUiOiJhY3RpdmUiLCJhdmF0YXJfdXJsIjoia"
                    "HR0cHM6Ly9nbC1zdGF0aWMuZ2xvYmFsLnNzbC5mYXN0bHkubmV0L3VwbG9hZHMvLS9zeXN0ZW0v"
                    "dXNlci9hdmF0YXIvMzg1MjcvZG9nZS5qcGVnIiwid2ViX3VybCI6Imh0dHBzOi8vZ2l0bGFiLmN"
                    "vbS9kb2dlZGV2In0sImFzc2lnbmVlIjpudWxsLCJ1c2VyX25vdGVzX2NvdW50IjowLCJ1cHZvdG"
                    "VzIjowLCJkb3dudm90ZXMiOjAsImR1ZV9kYXRlIjpudWxsLCJjb25maWRlbnRpYWwiOmZhbHNlL"
                    "CJ3ZWlnaHQiOm51bGwsIndlYl91cmwiOiJodHRwczovL2dpdGxhYi5jb20vZG9nZWRldi90ZXN0"
                    "LWNsaS1naXRsYWIvaXNzdWVzLzEiLCJ0aW1lX3N0YXRzIjp7InRpbWVfZXN0aW1hdGUiOjAsInR"
                    "vdGFsX3RpbWVfc3BlbnQiOjAsImh1bWFuX3RpbWVfZXN0aW1hdGUiOm51bGwsImh1bWFuX3RvdG"
                    "FsX3RpbWVfc3BlbnQiOm51bGx9fV0="],
                   :chunked? false,
                   :headers #vcr-clj/clj-http-header-map
                   {"Cache-Control" "max-age=0, private, must-revalidate",
                    "Connection" "close",
                    "Content-Length" "2609",
                    "Content-Type" "application/json",
                    "Date" "Thu, 21 Sep 2017 13:57:22 GMT",
                    "Etag" "W/\"a8eb2bf6dd8a8014f682f08e12954e84\"",
                    "Link" "<https://gitlab.com/api/v4/projects/4202845/issues?id=4202845&order_by=created_at&page=1&per_page=20&sort=desc&state=all>; rel=\"first\", <https://gitlab.com/api/v4/projects/4202845/issues?id=4202845&order_by=created_at&page=1&per_page=20&sort=desc&state=all>; rel=\"last\"",
                    "RateLimit-Limit" "600",
                    "RateLimit-Observed" "4",
                    "RateLimit-Remaining" "596",
                    "Server" "nginx",
                    "Strict-Transport-Security" "max-age=31536000",
                    "Vary" "Origin",
                    "X-Frame-Options" "SAMEORIGIN",
                    "X-Next-Page" "",
                    "X-Page" "1",
                    "X-Per-Page" "20",
                    "X-Prev-Page" "",
                    "X-Request-Id" "062b9d0a-afe3-44dc-a698-c88f9d569be0",
                    "X-Runtime" "0.408633",
                    "X-Total" "3",
                    "X-Total-Pages" "1"},
                   :length 2609,
                   :protocol-version {:major 1, :minor 1, :name "HTTP"},
                   :reason-phrase "OK",
                   :repeatable? false,
                   :status 200,
                   :streaming? true,
                   :trace-redirects []},
          :var-name "clj-http.core/request"}],
 :recorded-at #inst "2017-09-21T13:56:56.395-00:00"}

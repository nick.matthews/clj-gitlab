(ns clj-gitlab.core
  (:require [clj-http.client :as client]
            [cheshire.core :refer :all]))

(def ^:dynamic gitlab-host "https://gitlab.com")
(def api-root "/api/v4")

(defmacro with-url
  "Runs the specified body with a different gitlab-host.
  This allows the API to be used for private gitlab servers."
  [new-url & body]
  `(binding [gitlab-host ~new-url]
     ~@body))

(defn- headers [options]
  (if-let [token (get options :token)]
    {"PRIVATE-TOKEN" token}
    {}))

(defn api-url [endpoint]
  (str gitlab-host api-root endpoint))

(defn parse-response [response]
  (parse-string
    (get response :body)))

(defn api-get
  ([endpoint] (api-get endpoint {}))
  ([endpoint options]
   (parse-response
     (client/get
       (api-url endpoint)
       {:headers (headers options)
        :accept :json}))))

(defn api-post [endpoint params options]
  (parse-response
    (client/post
      (api-url endpoint)
      {:headers (headers options)
       :form-params params
       :accept :json})))

(defn api-put [endpoint params options]
  (parse-response
    (client/put
      (api-url endpoint)
      {:headers (headers options)
       :form-params params
       :accept :json})))

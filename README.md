# clj-gitlab

[![pipeline status](https://gitlab.com/dzaporozhets/clj-gitlab/badges/master/pipeline.svg)](https://gitlab.com/dzaporozhets/clj-gitlab/commits/master)
[![Clojars Project](https://img.shields.io/clojars/v/clj-gitlab.svg)](https://clojars.org/clj-gitlab)

Clojure client for GitLab API. Work in progress.

## Usage

Add `[clj-gitlab "0.1.0"]` to your `:dependencies` in your `project.clj` file.

In app:

```clojure
(ns your-app.core
  (:require [clj-gitlab.projects :as projects]))

(defn foo []
  (projects/project "123"))

(defn bar []
  (projects/update-project "123" {:description "Wow"} {:token "xxxxx"}))
```

In repl:

```
$-> (require '[clj-gitlab.projects :as projects])

# Without authentication
$-> (projects/project "123")

# With Private or Personal token
$-> (projects/project "123" {:token "XXXXXXXXXXXXX"})
```

## Progress

| HTTP Method  | GitLab API  | clj-gitlab  |
|---|---|---|
| GET | `/projects` | `projects/projects` |
| GET | `/projects/:id` | `projects/project`|
| POST| `/projects/:id` | `projects/create-project`|
| PUT | `/projects/:id` | `projects/update-project`|
| GET | `/user/:user_id/projects` | `projects/user-projects`|
| GET | `/projects/:id/issues` | `issues/project-issues`|
| POST| `/projects/:id/issues` | `issues/create-issue`|
| PUT | `/projects/:id/issues` | `issues/update-issue`|
| GET | `/projects/:id/issues/:iid` | `issues/issue`|
| GET | `/issues` | `issues/issues`|
| GET | `/projects/:id/repository/commits` | `commits/commits`|
| GET | `/projects/:id/repository/commits/:sha` | `commits/commit`|
| GET | `/projects/:id/repository/commits/:sha/diff` | `commits/commit-diff`|
| GET | `/projects/:id/repository/commits/:sha/comments` | `commits/commit-comments`|
| GET | `/projects/:id/repository/tree` | `files/tree`|

For not implemented calls you can use generic core method:

```
$-> (require '[clj-gitlab.core :as core])
$-> (core/api-get "/projects/1234/issues" {:token "XXXXXXX"})
$-> (core/api-post "/projects/1234/issues" {:title "Does not work"} {:token "XXXXXXX"})
```

## License

MIT. See LICENSE file
